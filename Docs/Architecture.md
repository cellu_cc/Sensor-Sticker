# Sensor Skin Architecture

The sensor skin is the component that performs the sensing of the pressure.

## Working Group
The working group model involves partitioning the sensor network into groups of local sensor nodes, which then collate their information in order to transmit a refined result. The nodes nominate a leader to communicate results back to the main board. Each sensor node then communicates with its nearest neighbors in order to asses the pressure gradient in its local vicinity. It then transmits this gradient information to the nominated leader, which then collates the information and transmits an information packet containing the refined information of the working group to the central processor.

A sensor node can be a member of multiple working groups, but can be leader of only one group.

A sensor node then needs to know the positions of its nearest neighbors.

### Questions
 1. Should working group partitions be gerrymandered or compact?
   * Is it possible to apply an emergent algorithm to create working groups? Some sort of nucleation?
 2. Does each node need to know the physical locations of its neighbors?
   * is it possible to reconstruct this information from a map later, given only local sensor network groups and?
   * likely need on board FRAM/permanent memory to store these state data.

Packet information: Working group ID. Nearest Neighbors' state information


## Central Connection
Transmission speed of the central processor is 480 MBit/sec. That's 240 MBit/sec/half

Each processor operates 2 uart serial connections at 115200 baud or 230 kBit/sec. If each is operating at full speed, then ten nodes transmits a total of 2.4 Mbit/sec. Therefore, a 3.5 mHz I2C connection might be sufficient. Alternatively
