# Sensor-Sticker

Minimal hardware for distributed sensing in aircraft, etc.

## Version

**Version 0:** 
* Minimal sensor size, irregular boards
* Ordered 2016-12-30

**Version 1:** 
* Constant sensor button size
* (TODO) Ensure that distance between connector edge and sensor centerpoint is 1.7 mm
* (TODO) Redo jtag pinout for Atmel ICE programmer
* Redo DSP310 Pinout to make assembly clearer
* Ordered 2017-01-19

**Version 1.5:**
* ATSAMD11 and PSoC4000 hosting

**Programming Bootloader with Atmel ICE***
1) Open atmel studio
2) Tools > device programming
3) Select atmel ice as the tool
4) The chip is atsamd21g18a
5) Interface is swd
6) Click apply
7) Read device signature
8) Go to memories and upload the hex file