# Project Timeline

A reverse-chronological description of the tasks to be done for the sensor project, starting with the wind tunnel test in Mid July.


  1. **July 14**: Testing begins
  1. **July 7** Receipt of V2 Boards, batch 2 (250). Integration into half span.
  1. **June 30**: Receipt of V2 boards, Batch 1 (250). Integration into half-span.
  1. **June 23**: Work on large-scale integration infrastructure
    * Objectives:
      1. Show the ability to access nodes over the network through the sink using the computer UI
      1. Set up firmware for debugging ~10^2 nodes simultaneously.
  1. **June 16**: Demonstration of consensus filter flow algorithm using ~10 boards.
    * Objectives:
      1. Show functionality on a physical model (MADCAT or MADCAT fragment)
      1. Demonstrate first pass sink/data acquisition software in simulated flow conditions
  1. **June 9**: Development of physical sensor filters
    * Objectives:
      1. Demonstrate efficient calculation of orientation using the Madgwick filter
      1. Demonstrate pressure field interpolation and circulation calculation between nodes
      1. Assessment of prototype boards, order first large batch of V2 boards with a 2 week lead.
  1. **June 2**: Demonstrate key network algorithms
    * Objectives:
      1. nodes are able to synchronize time across working group
        * demonstration where nodes begin at different times and synch their LED pulses
      1. nodes are able to elect a leader that collates & communicates results
      1. nodes can identify a failed leader and reelect
  1. **Out of town May 25-May 29**
  1. **May 24**: Finalize PHY layer infrastructure
    * Objectives:
      1. Test whether differential or flow control produces "better" results than a typical UART. (better given benchmarks determined in previous objective)
      1. Order new prototype boards that use revised PHY layer hardware.
  1. **May 19**: Establish base PHY layer benchmarks for V1 Boards
    * Objectives:
      1. Show impact of message size, baud rate, overhead (secondary calculations) on key performance parameters of a ring oscillator test
      1. Key performance parameters: latency, packet loss (%), resilience.

# Descriptions

## Testing

The big two weeks...

## Half-Span/Full-Span integration

The goal here is to take a half-span set of boards and successfully integrate them into a half span. This includes the development of methodologies for incrementally testing, verifying, and expanding the sensor infrastructure.

## Large-Scale Integration Infrastructure

Using the MADCAT fragment as a test, the next step is to refine the sink firmware and computer software. In particular, the focus will be on streamlining the network setup, debug, and configuration tasks so that a 500-node integration goes as smoothly as possible.

## Consensus Filter Flow Algorithm

With network functionality and basic sensor filters in place, the next step is to verify the functionality in a simulated flight condition. In order to simplify debugging, only a few sensors (~10) on a small wing will be used. The goal here is to demonstrate the ability to transmit refined state information back to a rudimentary sink.

## Physical Sensor Filters

Nodes need to process and transmit refined state information collected by the on-board sensors. The objective here is to develop and demonstrate key algorithms that perform this refinement. This includes implementing MARG-specific filters in order to find orientation, as well as custom filters that allow the calculation of the gradient of the pressure field.

## Key network algorithms

Once PHY layer infrastructure has been finalized, the next step is to demonstrate mid-level functionality between networks consisting of several nodes, including key background tasks that will provide accurate results in future calculations

## PHY Layer infrastructure

  There are currently multiple options for the PHY layer, including UARTs, USARTS, UART over RS-485, and d-I2C (differential I2C). Each provides their own advantages and disadvantages.

  * UART (Universal Asynchronous Receiver/Transmitter): advantages include low overhead, full-duplex, and hardware support. disadvantages include low speed, hardware support is half duplex, and no flow control.
  * USART (Synchronous UART): adds two additional lines to UART in exchange for providing a means by which each MCU can indicate whether it is ready to receive another byte.
  * UART over RS-485: is a UART that uses a differential signal instead of a common ground. Advantages include higher speed (possibly), better resistance to EMI, and therefore longer range. Disadvantages include the requirement of additional hardware, and the MCU support is only half-duplex.
  * d-I2C (differential I2C): is similar to RS485. Advantages include that it can operate up to 1 Mbps. Disadvantages include a large number of wires, and software-level arbitration in order to be used for full bidirectional comms

## PHY Layer Benchmarks

  A robust, low-latency network is a critical component of this work. Being able to assess the effectiveness of a given network hardware solution will allow the accurate comparison of different options and the selection of a network infrastructure that addresses the key experimental objectives.
