EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:IMU_Mount-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3600 2650 2    30   Input ~ 0
AI2
Text GLabel 3000 2650 0    30   Input ~ 0
AI1
Text GLabel 3600 2550 2    30   Input ~ 0
GI2
Text GLabel 3000 2550 0    30   Input ~ 0
GI1
Text GLabel 3600 2250 2    30   Input ~ 0
3V3
Text GLabel 3000 2350 0    30   Input ~ 0
GND
Text GLabel 3600 2350 2    30   Input ~ 0
SCL_5V
Text GLabel 3000 2450 0    30   Input ~ 0
SDA_5V
Text GLabel 3600 2450 2    30   Input ~ 0
RST_5V
Text GLabel 3000 2250 0    30   Input ~ 0
3V3
Text GLabel 3600 1650 2    30   Input ~ 0
3V3
Text GLabel 3000 1450 0    30   Input ~ 0
GND
Text GLabel 3600 1550 2    30   Input ~ 0
SCL_5V
Text GLabel 3000 1650 0    30   Input ~ 0
SDA_5V
Text GLabel 3600 1450 2    30   Input ~ 0
RST_5V
$Comp
L CONN_02X05 P1
U 1 1 5973B07F
P 3300 1650
F 0 "P1" H 3300 1950 50  0000 C CNN
F 1 "CONN_02X05" H 3300 1350 50  0000 C CNN
F 2 "_CSL:FFC_DIP_ADAPTER" H 3300 450 60  0001 C CNN
F 3 "" H 3300 450 60  0000 C CNN
	1    3300 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1450 3050 1450
Wire Wire Line
	3050 1650 3000 1650
Wire Wire Line
	3600 1650 3550 1650
Wire Wire Line
	3600 1550 3550 1550
Wire Wire Line
	3600 1450 3550 1450
$Comp
L CONN_02X05 P2
U 1 1 5973B253
P 3300 2450
F 0 "P2" H 3300 2750 50  0000 C CNN
F 1 "CONN_02X05" H 3300 2150 50  0000 C CNN
F 2 "_CSL:AF_IMU_FXOS8700+FXAS21002" H 3300 1250 60  0001 C CNN
F 3 "" H 3300 1250 60  0000 C CNN
	1    3300 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2250 3050 2250
Wire Wire Line
	3000 2350 3050 2350
Wire Wire Line
	3000 2450 3050 2450
Wire Wire Line
	3000 2550 3050 2550
Wire Wire Line
	3000 2650 3050 2650
Wire Wire Line
	3550 2650 3600 2650
Wire Wire Line
	3550 2550 3600 2550
Wire Wire Line
	3550 2450 3600 2450
Wire Wire Line
	3550 2350 3600 2350
Wire Wire Line
	3550 2250 3600 2250
$Comp
L CONN_01X05 P3
U 1 1 5973B512
P 2800 900
F 0 "P3" H 2800 1200 50  0000 C CNN
F 1 "CONN_01X05" V 2900 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm_SMD_Pin1Left" H 2800 900 60  0001 C CNN
F 3 "" H 2800 900 60  0000 C CNN
	1    2800 900 
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 P4
U 1 1 5973B53C
P 3850 900
F 0 "P4" H 3850 1200 50  0000 C CNN
F 1 "CONN_01X05" V 3950 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm_SMD_Pin1Right" H 3850 900 60  0001 C CNN
F 3 "" H 3850 900 60  0000 C CNN
	1    3850 900 
	1    0    0    -1  
$EndComp
Text GLabel 2550 700  0    30   Input ~ 0
GND
Text GLabel 2550 900  0    30   Input ~ 0
SDA_5V
Text GLabel 3600 700  0    30   Input ~ 0
RST_5V
Text GLabel 3600 800  0    30   Input ~ 0
SCL_5V
Text GLabel 3600 900  0    30   Input ~ 0
3V3
Wire Wire Line
	3650 700  3600 700 
Wire Wire Line
	3650 800  3600 800 
Wire Wire Line
	3650 900  3600 900 
Wire Wire Line
	2550 900  2600 900 
Wire Wire Line
	2550 700  2600 700 
$EndSCHEMATC
