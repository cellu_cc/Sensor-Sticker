# Sensor Stickers

The sensor nodes should be able to collect sensor data and transmit those data over a network to a central sink that then sends them to a computer for storage or display.

Bonus functionalities:
    * The network is composed of the nodes running APA
    * The nodes perform some local calculation on the data in order to simplify them or produce more refined measurements.
        * this includes pressure field calculations, where neighbor
    * The nodes perform a full calculation of important operation parameters in real-time
        * this can include circulation, drag coefficient, lift coefficient

## Tasks
    * Design a software core that successfully initializes the desired peripherals in the desired configuration
        * Target CPU speed is 48 MHz using the DFLL that runs off the external 32kHz crystal
    * implement an RTOS on the boards
        * FreeRTOS is available pre-built on Atmel SAM boards
    * develop a software protocol for serial communication between boards using the Asynchronous Packet Automaton
        * use both flow control and the RTOS to coordinate RX data to the input buffer
        * use DMA controller to tie output buffer to the TX register on the UART peripheral
    * Integrate existing Arduino libraries into the Atmel library environment, making modifications to C++ code where needed to make it pure C (replace classes with structs)
    * develop a library for NVM access of the flash memory in the microcontroller
        * include a specification for microcontroller state composed of current operation modes, local network characteristics, and data relevant to the node in question.
            * include the ability to read and write this state
        * include the ability to locally store and retrieve sensor data
            * characterize the amount of time required to write a given amount of data to the flash memory, and choose the frequency of system writes using that time
