# Layer Mapping

Top Layer: F
Bottom Layer: B
    Cu: Copper
    Mask: Soldermask
    Paste: Solder Paste
    SilkS: Silkscreen
    Adhes: Adhesive

Board Edge: Edge.Cuts

Drill File: .drl

Board Thickness: 0.3125 (1/32) inches

Quality Specifications: IPC-A-610, Class 2
