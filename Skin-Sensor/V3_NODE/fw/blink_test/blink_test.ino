#include "wiring_private.h"

#define GRE_LED 30 //PB23
#define RED_LED 31 //PB22
#define BLU_LED 19 //PB02


#define BAUDRATE 115200
#define UARTCONFIG SERIAL_8N1

void setup() {
  // put your setup code here, to run once:
  pinMode(RED_LED, OUTPUT);
  pinMode(BLU_LED, OUTPUT);
  pinMode(GRE_LED, OUTPUT);

  digitalWrite(RED_LED, LOW);
  digitalWrite(GRE_LED, LOW);
  digitalWrite(BLU_LED, LOW);

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(GRE_LED, LOW);
  //digitalWrite(RED_LED, LOW);
  delay(1000);
  digitalWrite(GRE_LED, HIGH);
  //digitalWrite(RED_LED, HIGH);
  delay(1000);
}


SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
