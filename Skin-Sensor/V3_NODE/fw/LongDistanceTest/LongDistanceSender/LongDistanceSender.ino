#include "wiring_private.h"

#define SerialUSB SERIAL_PORT_USBVIRTUAL

#define APA_RX_PAD SERCOM_RX_PAD_1
#define APA_TX_PAD UART_TX_RTS_CTS_PAD_0_2_3

#define APA1_TX_PIN   (4ul)            //A08
#define APA1_RX_PIN   (3ul)            //A09
#define APA1_RTS_PIN  (1ul)            //A10
#define APA1_CTS_PIN  (0ul)            //A11
#define APA1_PINMODE PIO_SERCOM_ALT   
#define APA1_SERCOM (&sercom2)    

#define BAUDRATE 1000000
#define UARTCONFIG SERIAL_8N1


Uart u1(APA1_SERCOM, APA1_RX_PIN, APA1_TX_PIN, APA_RX_PAD, APA_TX_PAD); 

uint8_t sendval = 0;
void setup() {
  SerialUSB.begin(115200);
  // put your setup code here, to run once:
  pinPeripheral( APA1_RX_PIN, APA1_PINMODE); //PA09|D3
  pinPeripheral( APA1_TX_PIN, APA1_PINMODE); //PA08|D4
  pinPeripheral(APA1_RTS_PIN, APA1_PINMODE); //PA10|D1
  pinPeripheral(APA1_CTS_PIN, APA1_PINMODE); //PA11|D0

  APA1_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA1_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA1_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA1_SERCOM->enableUART();
  sendval = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  u1.write(sendval);
  sendval++;
  delay(1000);
  
}

void SERCOM2_Handler(){
  u1.IrqHandler();
}

SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
