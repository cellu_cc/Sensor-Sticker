#include "wiring_private.h"

#define SerialUSB SERIAL_PORT_USBVIRTUAL

#define APA_RX_PAD SERCOM_RX_PAD_1
#define APA_TX_PAD UART_TX_RTS_CTS_PAD_0_2_3

#define APA0_TX_PIN  (17ul)            //A04
#define APA0_RX_PIN  (18ul)            //A05
#define APA0_RTS_PIN  (8ul)            //A06
#define APA0_CTS_PIN  (9ul)            //A07
#define APA0_PINMODE PIO_SERCOM_ALT
#define APA0_SERCOM (&sercom0)
           
#define APA1_TX_PIN   (4ul)            //A08
#define APA1_RX_PIN   (3ul)            //A09
#define APA1_RTS_PIN  (1ul)            //A10
#define APA1_CTS_PIN  (0ul)            //A11
#define APA1_PINMODE PIO_SERCOM_ALT   
#define APA1_SERCOM (&sercom2)          

#define APA2_TX_PIN  (22ul)            //A12
#define APA2_RX_PIN  (38ul)            //A13
#define APA2_RTS_PIN  (2ul)            //A14
#define APA2_CTS_PIN  (5ul)            //A15
#define APA2_PINMODE PIO_SERCOM_ALT
#define APA2_SERCOM (&sercom4)

#define APA3_TX_PIN  (11ul)            //A16
#define APA3_RX_PIN  (13ul)            //A17
#define APA3_RTS_PIN (10ul)            //A18
#define APA3_CTS_PIN (12ul)            //A19
#define APA3_PINMODE PIO_SERCOM
#define APA3_SERCOM (&sercom1)

#define BAUDRATE 115200
#define UARTCONFIG SERIAL_8N1

#define MESSAGE_LENGTH (36)

Uart u0(APA0_SERCOM, APA0_RX_PIN, APA0_TX_PIN, APA_RX_PAD, APA_TX_PAD); 
Uart u1(APA1_SERCOM, APA1_RX_PIN, APA1_TX_PIN, APA_RX_PAD, APA_TX_PAD); 
Uart u2(APA2_SERCOM, APA2_RX_PIN, APA2_TX_PIN, APA_RX_PAD, APA_TX_PAD); 
Uart u3(APA3_SERCOM, APA3_RX_PIN, APA3_TX_PIN, APA_RX_PAD, APA_TX_PAD); 

char inmessage[255];
char outmessage[255];

uint8_t inmesslen,outmesslen;

uint32_t beforetime, aftertime;
float latency;
int latencycount;

void setup() {
  SerialUSB.begin(115200);
  // put your setup code here, to run once:
  //begin U1
  pinPeripheral( APA0_RX_PIN, APA0_PINMODE); //PA09|D3
  pinPeripheral( APA0_TX_PIN, APA0_PINMODE); //PA08|D4
  pinPeripheral(APA0_RTS_PIN, APA0_PINMODE); //PA10|D1
  pinPeripheral(APA0_CTS_PIN, APA0_PINMODE); //PA11|D0

  APA0_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA0_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA0_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA0_SERCOM->enableUART();

  pinPeripheral( APA1_RX_PIN, APA1_PINMODE); //PA09|D3
  pinPeripheral( APA1_TX_PIN, APA1_PINMODE); //PA08|D4
  pinPeripheral(APA1_RTS_PIN, APA1_PINMODE); //PA10|D1
  pinPeripheral(APA1_CTS_PIN, APA1_PINMODE); //PA11|D0

  APA1_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA1_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA1_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA1_SERCOM->enableUART();

  pinPeripheral( APA2_RX_PIN, APA2_PINMODE); //PA09|D3
  pinPeripheral( APA2_TX_PIN, APA2_PINMODE); //PA08|D4
  pinPeripheral(APA2_RTS_PIN, APA2_PINMODE); //PA10|D1
  pinPeripheral(APA2_CTS_PIN, APA2_PINMODE); //PA11|D0

  APA2_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA2_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA2_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA2_SERCOM->enableUART();

  pinPeripheral( APA3_RX_PIN, APA3_PINMODE); //PA09|D3
  pinPeripheral( APA3_TX_PIN, APA3_PINMODE); //PA08|D4
  pinPeripheral(APA3_RTS_PIN, APA3_PINMODE); //PA10|D1
  pinPeripheral(APA3_CTS_PIN, APA3_PINMODE); //PA11|D0

  APA3_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA3_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA3_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);
  
  APA3_SERCOM->enableUART();

  inmesslen = 0;
  outmesslen = 0;
  latencycount = 0;

  beforetime = 0;
  latency = 0;
}

void loop() {
  //checkUart(&u0);
  checkUart(&u1);
  //checkUart(&u2);
  //checkUart(&u3);
  if(SerialUSB.available() > 0){
    if(SerialUSB.read() == 'p'){
      SerialUSB.println("Pinging");
      outmesslen = 5;
      outmessage[0] = '{';
      outmessage[1] = '^';
      outmessage[2] = '|';
      outmessage[3] = 'p';
      outmessage[4] = '}';
      /*
      for(int i = 0; i < MESSAGE_LENGTH; i++){
        outmessage[i+4] = '!';
      }
      outmessage[4+MESSAGE_LENGTH] = '}';
      */
    }
    if(SerialUSB.read() == 's'){
      
    }
  }
  if(latencycount == 10){
    latencycount = 0;
    SerialUSB.print("Latency: ");
    SerialUSB.print(latency);
    SerialUSB.println("us");
    latency = 0;
  }
}

void processPacket(){
  aftertime = micros();
  //SerialUSB.println("processing packet");
  latency = latency + 0.1*(aftertime-beforetime);
  //SerialUSB.println(0.1*(aftertime-beforetime));
  latencycount++;
  beforetime = micros();

  for(int i =0; i < inmesslen; i++)
    outmessage[i] = inmessage[i];
    
  outmesslen = inmesslen;
  inmesslen = 0;
}

void SERCOM0_Handler(){
  u0.IrqHandler();
}

void SERCOM2_Handler(){
  u1.IrqHandler();
}

void SERCOM4_Handler(){
  u2.IrqHandler();
}

void SERCOM1_Handler(){
  u3.IrqHandler();
}

void checkUart(Uart *u){
  if(u->available() > 0){
    bool searching = true;
    while(u->available()&&searching){
      //SerialUSB.print("Reading Byte: ");
      inmessage[inmesslen] = u->read();
      //SerialUSB.println((char)inmessage[inmesslen]);
      inmesslen++;
      if(inmessage[inmesslen-1] == '}'){
        searching = false;
        processPacket();
      }
    }
  }
  
  if(outmesslen > 0){
    //SerialUSB.println("Writing Message");
    u->write(outmessage,outmesslen);
    outmesslen = 0;
    //SerialUSB.println("Wrote message");
  }
}

SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
