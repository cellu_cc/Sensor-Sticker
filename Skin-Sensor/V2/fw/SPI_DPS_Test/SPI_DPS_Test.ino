#include <SPI.h>
#include "wiring_private.h"

#define SerialUSB SERIAL_PORT_USBVIRTUAL

//Manual UART defines
#define APA_RX_PAD SERCOM_RX_PAD_1
#define APA_TX_PAD UART_TX_PAD_0

#define APA0_TX_PIN  (17ul)            //A04
#define APA0_RX_PIN  (18ul)            //A05
#define APA0_RTS_PIN  (8ul)            //A06
#define APA0_CTS_PIN  (9ul)            //A07
#define APA0_PINMODE PIO_SERCOM_ALT
#define APA0_SERCOM (&sercom0)

#define BAUDRATE 115200
#define UARTCONFIG SERIAL_8N1

//SPI Port Defines

#define SPI_SCK  21
#define SPI_MISO  6
#define SPI_MOSI 20

#define CS_0 7   //PA21 - Pin 30 on the D21G
#define CS_1 16  //PB09 - Pin 8 on the D21G
#define CS_2 24  //PB11 - Pin 20 on the D21G
#define CS_3 14  //PA02 - Pin 3 on the D21G


// Serial Object Instantiation
Uart u0(APA0_SERCOM, APA0_RX_PIN, APA0_TX_PIN, APA_RX_PAD, APA_TX_PAD);

SPIClass SPI1 (&sercom3, SPI_MISO, SPI_SCK, SPI_MOSI, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_2);

//Scale Factors for DPS310

const float pscalefactor = 1.0/524288.0; //1 OSR
const float tscalefactor = 1.0/7864320.0;//8 OSR

int coeff0[9];
int coeff1[9];
int coeff2[9];
int coeff3[9];

//Temp vars
uint32_t b4_transfer = 0;
uint32_t aftr_transfer = 0;
uint32_t val0 = 0;
uint32_t val1 = 0;
uint32_t val2 = 0;
uint32_t val3 = 0;


void setup() {
  SerialUSB.begin(115200);
  SPI1.begin();

  pinPeripheral( APA0_RX_PIN, APA0_PINMODE); //PA09|D3
  pinPeripheral( APA0_TX_PIN, APA0_PINMODE); //PA08|D4

  APA0_SERCOM->initUART(UART_INT_CLOCK, SAMPLE_RATE_x16, BAUDRATE);
  APA0_SERCOM->initFrame(extractCharSize(UARTCONFIG), LSB_FIRST, extractParity(UARTCONFIG), extractNbStopBit(UARTCONFIG));
  APA0_SERCOM->initPads(APA_TX_PAD, APA_RX_PAD);

  APA0_SERCOM->enableUART();

  // put your setup code here, to run once:
  pinPeripheral(SPI_SCK, PIO_SERCOM);
  pinPeripheral(SPI_MOSI, PIO_SERCOM);
  pinPeripheral(SPI_MISO, PIO_SERCOM_ALT);

  pinMode(CS_0, OUTPUT);
  pinMode(CS_1, OUTPUT);
  pinMode(CS_2, OUTPUT);
  pinMode(CS_3, OUTPUT);

  digitalWrite(CS_0, HIGH);
  digitalWrite(CS_1, HIGH);
  digitalWrite(CS_2, HIGH);
  digitalWrite(CS_3, HIGH);

  uint8_t tmp = 0;

  //Reset Circuit

  writeRegister(0x0C, 0x09, CS_0);
  writeRegister(0x0C, 0x09, CS_1);
  writeRegister(0x0C, 0x09, CS_2);
  writeRegister(0x0C, 0x09, CS_3);

  delay(1000);

  writeRegister(0x06, 0x70, CS_0);
  writeRegister(0x07, 0x33, CS_0);
  writeRegister(0x08, 0x07, CS_0);

  readCoeff(coeff0, CS_0);

  writeRegister(0x06, 0x70, CS_1);
  writeRegister(0x07, 0x33, CS_1);
  writeRegister(0x08, 0x07, CS_1);

  readCoeff(coeff1, CS_1);

  writeRegister(0x06, 0x70, CS_2);
  writeRegister(0x07, 0x33, CS_2);
  writeRegister(0x08, 0x07, CS_2);

  readCoeff(coeff2, CS_2);

  writeRegister(0x06, 0x70, CS_3);
  writeRegister(0x07, 0x33, CS_3);
  writeRegister(0x08, 0x07, CS_3);

  readCoeff(coeff3, CS_3);
}

void loop() {
  val0 = readCalibratedPressure(coeff0, CS_0);
  val1 = readCalibratedPressure(coeff1, CS_1);
  val2 = readCalibratedPressure(coeff2, CS_2);
  val3 = readCalibratedPressure(coeff3, CS_3);

  //SerialUSB.print("Time: ");
  //SerialUSB.print(aftr_transfer - b4_transfer);
  //SerialUSB.print("us \t 0x");
  /*
  u0.print(val0);
  u0.print(" ");
  u0.print(val1);
  u0.print(" ");
  u0.print(val2);
  u0.print(" ");
  u0.println(val3);
  */
  SerialUSB.print(val0);
  SerialUSB.print(" ");
  SerialUSB.print(val1);
  SerialUSB.print(" ");
  SerialUSB.print(val2);
  SerialUSB.print(" ");
  SerialUSB.println(val3);
  
  delay(10);

}

void writeRegister(uint8_t reg, uint8_t val, uint8_t cs){
  digitalWrite(cs, LOW);
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(reg);
  SPI1.transfer(val);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);
}

int toSignedInt(uint32_t value, uint8_t bitLength){
  int signedValue = value;
  if(value >> (bitLength-1))
    signedValue |= -1 << bitLength;
  return signedValue;
}


int readRawTemperature(uint8_t cs){
  uint32_t tval = 0;
  digitalWrite(cs, LOW);
  // put your main code here, to run repeatedly:
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x03 | (0x1 << 7));
  tval  = ((uint32_t)SPI1.transfer(0x00)) << 16;
  tval |= ((uint32_t)SPI1.transfer(0x00)) << 8;
  tval |=  (uint32_t)SPI1.transfer(0x00);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);

  return toSignedInt(tval, 24);
}

int readRawPressure(uint8_t cs){
  uint32_t tval = 0;
  digitalWrite(cs, LOW);
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x00 | (0x1 << 7));
  tval  = ((uint32_t)SPI1.transfer(0x00)) << 16;
  tval |= ((uint32_t)SPI1.transfer(0x00)) << 8;
  tval |=  (uint32_t)SPI1.transfer(0x00);
  SPI1.endTransaction();
  digitalWrite(cs, HIGH);

  return toSignedInt(tval, 24);
}

double readCalibratedTemp(int *coeff, uint8_t cs){
  int traw = readRawTemperature(cs);
  float scaledT = (float)traw*tscalefactor;
  return 0.5*coeff[0]+1.0*coeff[1]*scaledT;
}


double readCalibratedPressure(int *coeff, uint8_t cs){
  int traw = readRawTemperature(cs);
  int praw = readRawPressure(cs);
  double scaledT = (double)(toSignedInt(traw,24))*tscalefactor;
  double scaledP = (double)(toSignedInt(praw,24))*pscalefactor;
  return coeff[2] + scaledP*(coeff[3]+scaledP*(coeff[6]+scaledP*coeff[8])) + scaledT*coeff[4] + scaledT*scaledP*(coeff[5]+scaledP*coeff[7]);
}


void readCoeff(int *coeff, uint8_t cs){
  uint8_t calibration[18];
  uint32_t tcoeff[9];
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x00 | (0x1 << 7));

  digitalWrite(cs, LOW);
  // put your main code here, to run repeatedly:
  b4_transfer = micros();
  SPI1.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI1.transfer(0x10 | (0x1 << 7));
  for(uint8_t i = 0x10; i < 0x22; i++){
    calibration[i-0x10] = (uint8_t)(SPI1.transfer(0x00));
  }
  SPI1.endTransaction();
  aftr_transfer = micros();
  digitalWrite(cs, HIGH);

  tcoeff[0] = ((calibration[0])<<4) | ((calibration[1]>>4)&0x0F);
  tcoeff[1] = ((0x0F&calibration[1])<<8)|(calibration[2]);
  tcoeff[2] = (calibration[3]<<12)|(calibration[4]<<4)|(calibration[5]>>4);
  tcoeff[3] = ((0x0F&calibration[5])<<16)|(calibration[6]<<8)|(calibration[7]);
  tcoeff[4] = (calibration[8]<<8)|(calibration[9]);
  tcoeff[5] = (calibration[10]<<8)|(calibration[11]);
  tcoeff[6] = (calibration[12]<<8)|(calibration[13]);
  tcoeff[7] = (calibration[14]<<8)|(calibration[15]);
  tcoeff[8] = (calibration[16]<<8)|(calibration[17]);

  int size[9] = {12,12,20,20,16,16,16,16,16};
  for(uint8_t i = 0; i < 9; i++){
    coeff[i] = toSignedInt(tcoeff[i],size[i]);
  }

}

void SERCOM0_Handler(){
  u0.IrqHandler();
}


SercomNumberStopBit extractNbStopBit(uint16_t config)
{
  switch(config & HARDSER_STOP_BIT_MASK)
  {
    case HARDSER_STOP_BIT_1:
    default:
      return SERCOM_STOP_BIT_1;

    case HARDSER_STOP_BIT_2:
      return SERCOM_STOP_BITS_2;
  }
}

SercomUartCharSize extractCharSize(uint16_t config)
{
  switch(config & HARDSER_DATA_MASK)
  {
    case HARDSER_DATA_5:
      return UART_CHAR_SIZE_5_BITS;

    case HARDSER_DATA_6:
      return UART_CHAR_SIZE_6_BITS;

    case HARDSER_DATA_7:
      return UART_CHAR_SIZE_7_BITS;

    case HARDSER_DATA_8:
    default:
      return UART_CHAR_SIZE_8_BITS;
  }
}

SercomParityMode extractParity(uint16_t config)
{
  switch(config & HARDSER_PARITY_MASK)
  {
    case HARDSER_PARITY_NONE:
    default:
      return SERCOM_NO_PARITY;

    case HARDSER_PARITY_EVEN:
      return SERCOM_EVEN_PARITY;

    case HARDSER_PARITY_ODD:
      return SERCOM_ODD_PARITY;
  }
}
