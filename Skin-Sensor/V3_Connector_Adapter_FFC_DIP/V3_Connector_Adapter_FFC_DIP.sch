EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X05 P1
U 1 1 596D4357
P 5300 1350
F 0 "P1" H 5300 1650 50  0000 C CNN
F 1 "CONN_01X05" V 5400 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 5300 1350 60  0001 C CNN
F 3 "" H 5300 1350 60  0000 C CNN
	1    5300 1350
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X05 P3
U 1 1 596D437D
P 6500 1350
F 0 "P3" H 6500 1650 50  0000 C CNN
F 1 "CONN_01X05" V 6600 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 6500 1350 60  0001 C CNN
F 3 "" H 6500 1350 60  0000 C CNN
	1    6500 1350
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X05 P2
U 1 1 596D43A1
P 5950 1350
F 0 "P2" H 5950 1650 50  0000 C CNN
F 1 "CONN_02X05" H 5950 1050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 5950 150 60  0001 C CNN
F 3 "" H 5950 150 60  0000 C CNN
	1    5950 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1150 5700 1150
Wire Wire Line
	5500 1250 5700 1250
Wire Wire Line
	5500 1350 5700 1350
Wire Wire Line
	5500 1450 5700 1450
Wire Wire Line
	5500 1550 5700 1550
Wire Wire Line
	6200 1150 6300 1150
Wire Wire Line
	6200 1250 6300 1250
Wire Wire Line
	6200 1350 6300 1350
Wire Wire Line
	6200 1450 6300 1450
Wire Wire Line
	6200 1550 6300 1550
$EndSCHEMATC
