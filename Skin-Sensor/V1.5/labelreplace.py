import re
from shutil import move
from os import remove, close

tags = ['R','C','U','P']
oldfilename = "sensor_1.5_mult.kicad_pcb"
newfilename = "sensor_1.5_mult_rename.kicad_pcb"

for tag in tags:		
	regex = re.compile('(\s'+tag+"\d*\s)")
	with open(oldfilename) as oldf:
		with open(newfilename,'w') as newf:
			counter = 1
			for line in oldf:
				result = regex.search(line)
				if result != None and "fp_text" in line:
					print line.replace(result.group(0),' '+tag+str(counter)+' ')
					newf.write(line.replace(result.group(0),' '+tag+str(counter)+' '))
					counter += 1
				else:
					newf.write(line)
		#close(oldfilename)
	remove(oldfilename)
	move(newfilename,oldfilename)

