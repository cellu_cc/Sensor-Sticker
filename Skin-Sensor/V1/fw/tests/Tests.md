# Testing Schedule

Testing APA.

## Versions of APA to test:
  1. Arduino serial:
    * This version uses the default serial library. Writing to the buffer involves holding on a while loop until the DATA register clears.
    * possible bottleneck: the write function ties up the CPU until the data register clears.
  2. Arduino serial with interrupts
    * instead of holding on the while loop, the DATA clear event is tied to an interrupt that then lets the CPU know that it is clear to write another byte to the register. Allows the CPU to perform other tasks while waiting to write information to the buffer.
  3. Arduino serial with DMA
    * Serial object is tied to a DMA channel, such that the DATA full/empty event is tied to a automatic move of the data from register to queue.

## Tests to perform
