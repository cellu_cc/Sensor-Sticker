#include <apa.h>
#include <lp5521.h>

//Real time counter library
#include <RTC_CSL.h>

Lp5521 ledChip(0x32);

uint32_t curtime;
uint32_t ping_time, ping_send_time, ping_receive_time;
uint32_t ping_interval = 10;

int startuptime = 0;


boolean verbose_mode = true;

uint8_t rainbowtime = 0;

//Input queue structs
struct apa_queue *apa_iq_0, *apa_iq_1, *apa_iq_2, *apa_iq_s; 
//Output queue structs
struct apa_queue *apa_oq_0, *apa_oq_1, *apa_oq_2, *apa_oq_s;

//APA port structs
struct apa_port *apa_p_0, *apa_p_1, *apa_p_2, *apa_p_s; 

unsigned char temp_input, buf_len;
int tindex;

boolean debug = false;
boolean loop_debug = false;
boolean pinging = false;


void setup() {
  //Starting USB Serial
  SerialUSB.begin(230400);

  if (debug)
      SerialUSB.println("Starting LED Chip");
      
  // put your setup code here, to run once:
  ledChip.Begin();
  
  if(debug)
      Serial.println("Enabling LED");
    
  ledChip.Enable();

  ledChip.SetDriveCurrent(0, 10);
  ledChip.SetDriveCurrent(1, 10);
  ledChip.SetDriveCurrent(2, 10);
  
  if (debug){
      while(startuptime < 500){
          startuptime = startuptime + 1;
          ledChip.hum(startuptime%255);
          delay(5);
      }
      SerialUSB.println("Initializing APA Queues...");
  }

  ledChip.hum(0);

  apa_iq_0 = apa_init_queue();
  apa_iq_1 = apa_init_queue();
  apa_iq_2 = apa_init_queue();
  apa_iq_s = apa_init_queue();

  apa_oq_0 = apa_init_queue();
  apa_oq_1 = apa_init_queue();
  apa_oq_2 = apa_init_queue();
  apa_oq_s = apa_init_queue();

  if(debug){
    if(apa_iq_0 == NULL || apa_iq_1 == NULL || apa_iq_2 == NULL || apa_iq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for input queues");
    if(apa_oq_0 == NULL || apa_oq_1 == NULL || apa_oq_2 == NULL || apa_oq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for output queues");
  }

  
  if (debug)
    SerialUSB.println("Initializing APA Port Serials");
  APA0.begin(230400);
  APA1.begin(230400);
  APA2.begin(230400);
  
  if (debug)
    SerialUSB.println("Initializing APA Structs");
  //Initialize the ports with their IDs (using ASCII for now)
  apa_p_0 = apa_initialize('0');
  apa_p_1 = apa_initialize('1');
  apa_p_2 = apa_initialize('2');
  apa_p_s = apa_initialize('3');


  if (debug)
    SerialUSB.println("Configuring Local Network");
  //configure local network (links between port objects)
  apa_p_0->next_port = apa_p_1;
  apa_p_1->next_port = apa_p_2;
  apa_p_2->next_port = apa_p_s;  
  apa_p_s->next_port = apa_p_0;  


  if (debug)
    SerialUSB.println("Setting function pointers");
  //point to the local process function
  apa_p_0->process_packet = &local_process_packet;
  apa_p_1->process_packet = &local_process_packet;
  apa_p_2->process_packet = &local_process_packet;
  apa_p_s->process_packet = &local_process_packet;

  if (debug)
    SerialUSB.println("Setting queue pointers");

  // pointing port queue I/O pointers to the queue objects
      
  apa_p_0->packet_in = apa_iq_0;
  apa_p_1->packet_in = apa_iq_1;
  apa_p_2->packet_in = apa_iq_2;
  apa_p_s->packet_in = apa_iq_s;

  apa_p_0->packet_out = apa_oq_0;
  apa_p_1->packet_out = apa_oq_1;
  apa_p_2->packet_out = apa_oq_2;
  apa_p_s->packet_out = apa_oq_s;
}

void loop() {  
  //*
  if (debug && loop_debug)
      SerialUSB.println("Scanning APA Ports");
  
  apa_port_scan(apa_p_0);
  apa_port_scan(apa_p_1);
  apa_port_scan(apa_p_2);
  apa_port_scan(apa_p_s);
//*/

  if (debug && loop_debug)
      SerialUSB.println("Checking Input Queues");
      
  checkInputQueue(apa_iq_0,&APA0);
  checkInputQueue(apa_iq_1,&APA1);
  checkInputQueue(apa_iq_2,&APA2);    
  checkInputQueue(apa_iq_s,&SerialUSB);
  
  if (debug && loop_debug)
      SerialUSB.println("Checking Output Queues");
      
  checkOutputQueue(apa_p_0,&APA0);
  checkOutputQueue(apa_p_1,&APA1);
  checkOutputQueue(apa_p_2,&APA2);
  checkOutputQueue(apa_p_s,&SerialUSB);
  
  // put your main code here, to run repeatedly:
  curtime = millis();
  
  if(curtime - ping_time > ping_interval && pinging){
    ping(apa_p_0);
  }
 
}

void checkInputQueue(struct apa_queue *inqueue, Stream *serialobj){
  //Check if packet is ready (i.e. have we reached APA_END char yet?)
  //Designed to allow packets > 64 bytes (i.e. larger than serial buffer)
  if(!(inqueue->ready)){
    //Get the number of available bytes
    buf_len = serialobj->available();

    if(buf_len > 0){
      if(debug){
          SerialUSB.println("USB Data Available");
          SerialUSB.print("\tLength: ");
          SerialUSB.println((int)buf_len);
      }
    }
      
    //if the input queue is empty (nothing has been read in yet)...
    if(inqueue->len == 0){
      //then first search for APA_START
      tindex = 0;
      for(tindex = 0; tindex < buf_len; tindex++){
        temp_input = serialobj->read();
        //if the next char in the serial buffer is APA_START...
        if(temp_input == apa_start){
          if(debug)
            SerialUSB.println("Found start of packet");
          //Then set the proper value for buflen
          if(debug)
            SerialUSB.println(tindex);
            
          buf_len = buf_len - tindex; 
          tindex = buf_len+1; 
            
          if(debug)
            SerialUSB.println("Setting length");
            
          inqueue->len = 1;
          inqueue->packet[0] = apa_start;
        }
      }
      //check if we didn't find APA_START, then set the buffer length to 0.
      if(tindex != buf_len+1){
        buf_len = 0;
      }
    }
    //if the buffer is still larger than 0 then start reading in the data   
    if(buf_len > 0){
        for(tindex = 0; tindex < buf_len; tindex++){
          inqueue->packet[tindex+inqueue->len] = serialobj->read();
          if(inqueue->packet[tindex+inqueue->len] == apa_end){
            if(debug)
              SerialUSB.println("Reached end of packet");
            inqueue->len = inqueue->len+tindex+1;
            tindex = buf_len;
            inqueue->ready = true;
          }
        }
    }
    if(!(inqueue->ready)){
      inqueue->len = inqueue->len + buf_len;
    }
    else if(debug){
        SerialUSB.println("Input Queue Ready");
        SerialUSB.print("\tLength: ");
        SerialUSB.println((int)inqueue->len);
        SerialUSB.print("\tContents: ");
        SerialUSB.write((char *)inqueue->packet,inqueue->len);
        SerialUSB.println("\n");
    }
    }
}

void checkOutputQueue(struct apa_port *outport, Stream *serialobj){
    if(outport->packet_out->len > 0){
        if(debug){
            SerialUSB.print("Packet on Outqueue ");
            SerialUSB.print((char)outport->id);
            SerialUSB.print("\n");
            SerialUSB.print("\tLength: "); 
            SerialUSB.println(outport->packet_out->len);
            SerialUSB.print("\tPayload:\n\n "); 
            SerialUSB.write((char *)outport->packet_out->packet,outport->packet_out->len);
            SerialUSB.println("\n");
        }
        serialobj->write((char *)outport->packet_out->packet,outport->packet_out->len);
        outport->packet_out->len = 0;
    }
}

void local_process_packet(struct apa_port *port){
    switch(port->payload_out[0]){
      case 'p':
          rainbowtime = rainbowtime+1;
          ledChip.rainbow(rainbowtime);
          ping_receive_time = micros();
      
          SerialUSB.print("Ping: ");
          SerialUSB.print(ping_receive_time - ping_send_time);
          SerialUSB.println("us");
  
          ping_send_time = micros();
          
          port->path_out[0] = '^';
          port->path_out_length = 1;
          port->payload_out[0] = 'p';
          port->payload_out_length = 1;
          break;
      case 's':
          apa_p_0->path_out[0] = '^';
          apa_p_0->path_out_length = 1;
          apa_p_0->payload_out[0] = 'p';
          apa_p_0->payload_out_length = 1;
          
          port->path_out[0] = '^';
          port->path_out_length = 1;
          memcpy(port->payload_out,"Starting Ping Process",21);
          port->payload_out_length = 21;
          break;
    }
}

void ping(struct apa_port *port){
  if(port->path_out_length == 0){
    ping_time = curtime;
    ping_send_time = micros();
    
    port->payload_out[0] = 'p';
    port->payload_out_length = 1; 
    port->path_out[0] = '^';
    port->path_out_length = 1;
  }
}

void printPortStatus(struct apa_port *port){
  SerialUSB.print("Port ");
  SerialUSB.print((char)port->id);
  SerialUSB.println(":");
  SerialUSB.print("\tPath In (");
  SerialUSB.print((int)port->path_in_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->path_in,port->path_in_length);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPayload In (");
  SerialUSB.print((int)port->payload_in_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->payload_in, port->payload_in_length);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPath Out (");
  SerialUSB.print((int)port->path_out_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->path_out,port->path_out_length);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPayload Out (");
  SerialUSB.print((int)port->payload_out_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->payload_out,port->payload_out_length);
  SerialUSB.print("\n");
  SerialUSB.print("\n");
}

void apa_port_scan(struct apa_port *port) {
    //
    // port scan
    //
    struct apa_port *current_port;
    //
    // is an incoming packet waiting?
    //
    if (port->packet_in->ready) {
        //
        // yes, is the input empty?
        //
        if(debug)
          SerialUSB.println("APA Port Scan: Reading Input Packet");
        if (port->path_in_length == 0) {
            //
            // yes, accept the packet
            //
            if(debug)
              SerialUSB.println("APA Port Scan: Getting Packet:");
            apa_get_packet(port);
            if(debug)
              printPortStatus(port);

            //
            // was a packet received?
            //
            if (port->path_in_length != 0) {
                //
                // yes, route it
                //
                apa_route_packet(port);
                if(debug){
                  SerialUSB.println("APA Port Scan: Routing Packet:");
                  printPortStatus(port);
                }
            }
        }
        //
        // no, reject it
        //
    }
    //
    // is an input packet waiting?
    //
    if (port->path_in_length != 0) {
        //
        // yes, is the destination here?
        //
        if (port->destination == apa_here) {
            //
            // yes, is the output empty?
            //
            if(debug)
              SerialUSB.println("APA Destination Here");
            if (port->path_out_length == 0) {
                //
                // yes, move packet to output, reverse path, and process
                //
                if(debug)
                  SerialUSB.println("APA Port Scan: Processing Packet:");
                apa_move_packet(port,port);
                if(debug)
                  printPortStatus(port);
                port->process_packet(port);
            }
        }
        //
        // destination not here, is it a flood?
        //
        else if (port->destination == apa_flood) {
            //
            // yes, copy to other ports
            //
            //
            // NB: it is possible that the flood fails to propagate b/c the port already
            // has a packet in its input buffer.
            // The current version results in the packet just not being copied.
            //
            current_port = port->next_port;

            while (current_port->id != port->id) {
                apa_copy_packet(port,current_port);
                current_port = current_port->next_port;
            }
            //
            //clear the path and payload of the original port after flooding
            //
            port->path_in_length = 0;
            port->payload_in_length = 0;
        }
        //
        // destination not here or flood, loop over other ports
        //
        else {
            current_port = port->next_port;
            if(debug){
              SerialUSB.print("APA Destination: ");
              SerialUSB.println(port->destination);
            }
            while (1) {
                //
                // is current port the destination?
                //
                if (port->destination == current_port->id) {
                    //
                    // yes, is current port empty?
                    //
                    if (current_port->path_out_length == 0) {
                        //
                        // yes, move packet to current port
                        //
                        
                        if(debug)
                          SerialUSB.println("APA Port Scan: Moving Packet");
                          
                        apa_move_packet(port,current_port);
                        
                        if(debug)
                          printPortStatus(port);
                          
                        break;
                    }
                }
                //
                // no, back to same port?
                //
                if (port->id == current_port->id) {
                    //
                    // yes, discard invalid path
                    //
                    if(debug)
                      SerialUSB.println("APA Port Scan: Invalid Path");
                    port->path_in_length = 0;
                    break;
                }
                //
                // no, go to next port
                //
                current_port = current_port->next_port;
            }
        }
    }
    //
    // is an output packet waiting?
    //
    if (port->path_out_length != 0) {
        //
        // yes, try to send it
        //
        if(debug){
          SerialUSB.println("APA Port Scan: Putting Packet");
          printPortStatus(port);
        }
        apa_put_packet(port);
    }
}


