#include <lp5521.h>
#include <apa.h>
#include <apa_phy.h>
#include "network.h"
#include "debug.h"

//Real time counter library
#include <RTC_CSL.h>

//DMA Library
//#include <Adafruit_ZeroDMA.h>


//LED Chip object (for controlling RGB LED);
Lp5521 ledChip(0x32);

//Initialize RTC module
RTC_CSL rtc;

uint32_t curtime;
uint32_t ping_time, ping_send_time, ping_receive_time;
uint32_t ping_interval = 1000;
long avg_ping;
long ping_count;

// LED Timing variables (used for indicator LED functions)
int startuptime;
uint8_t rainbowtime = 0;

unsigned char temp_input, buf_len;
int tindex;

boolean debug = true;
boolean loop_debug = false;
boolean pinging = false;
boolean data_avail = false;

void setup() {
  //Starting USB Serial
  if (verbosity <= DEBUG)
    SerialUSB.begin(230400);

  if (verbosity <= DEBUG)
    SerialUSB.println("Starting RTC");
  rtc.begin();

  if (verbosity <= DEBUG)
    SerialUSB.println("Starting LED Chip");
  ledChip.Begin();

  if (verbosity <= DEBUG)
    Serial.println("Enabling LED");
  ledChip.Enable();

  //Configuring LED chip to have a lower drive current (less power/bright)
  ledChip.SetDriveCurrent(0, 10);
  ledChip.SetDriveCurrent(1, 10);
  ledChip.SetDriveCurrent(2, 10);

  //A pulsing indicator to show startup
  if (verbosity <= DEBUG) {
    while (startuptime < 500) {
      startuptime = startuptime + 1;
      ledChip.hum(startuptime % 255);
      delay(5);
    }
    SerialUSB.println("Initializing APA Queues...");
  }
  //Turn off the LED
  ledChip.hum(0);

  //Setting up the APA network
  initialize_network();

  if (verbosity <= DEBUG)
    SerialUSB.println("Setting function pointers");
  //point to the local process function
  apa_p_0->process_packet = &local_process_packet;
  apa_p_1->process_packet = &local_process_packet;
  apa_p_2->process_packet = &local_process_packet;
  apa_p_s->process_packet = &local_process_packet;

  avg_ping = 0;
  ping_count = 0;
}

void loop() {
  //*
  if (debug && loop_debug)
    SerialUSB.println("Scanning APA Ports");

  if (apa_port_scan(apa_p_0) != SUCCESS && debug) {
    SerialUSB.println("Failed Packet");
    printPortStatus(apa_p_0);
    apa_p_0->packet_in->len = 0;
    apa_p_0->packet_in->status = STATUS_EMPTY;
  }

  apa_port_scan(apa_p_1);
  apa_port_scan(apa_p_2);
  apa_port_scan(apa_p_s);
  //*/

  if (debug && loop_debug)
    SerialUSB.println("Checking Input Queues");


  checkInputQueue(apa_iq_s, &SerialUSB);


  if (debug && loop_debug)
    SerialUSB.println("Checking Output Queues");

  if (SerialUSB.available())
    checkOutputQueue(apa_p_s, &SerialUSB);

  APA0.checkOutputQueue();

  if (data_avail) {
    if (debug)
      SerialUSB.println("Data Available.");
    APA0.IrqHandler();
  }

  // put your main code here, to run repeatedly:
  curtime = millis();
  if (ping_count > 1000) {
    ping_count = 0;
    SerialUSB.println(avg_ping);
    avg_ping = 0;
  }
  /*

    if(curtime - ping_time > ping_interval && debug){
    ping_time = curtime;
    /*
    SerialUSB.print(apa_iq_s->len);
    SerialUSB.print(" ");
    SerialUSB.println((int)apa_iq_s->status);
    SerialUSB.print("Current Time:");
    SerialUSB.println(rtc.getCount());

    if(apa_p_0->path_in_length > 0 || apa_p_0->payload_in_length > 0){
      printPortStatus(apa_p_0);
    }
    }
  */
}

void checkInputQueue(struct apa_queue *inqueue, Stream *serialobj) {
  //Check if packet is ready (i.e. have we reached APA_END char yet?)
  //Designed to allow packets > 64 bytes (i.e. larger than serial buffer)
  if (inqueue->status > STATUS_FULL) {
    //Get the number of available bytes
    buf_len = serialobj->available();

    if (buf_len > 0) {
      if (debug) {
        SerialUSB.println("USB Data Available");
        SerialUSB.print("\tLength: ");
        SerialUSB.println((int)buf_len);
      }
    }

    //if the input queue is empty (nothing has been read in yet)...
    if (inqueue->status > STATUS_FILLING) {
      //then first search for APA_START
      tindex = 0;
      for (tindex = 0; tindex < buf_len; tindex++) {
        temp_input = serialobj->read();
        //if the next char in the serial buffer is APA_START...
        if (temp_input == apa_start) {
          if (debug)
            SerialUSB.println("Found start of packet");
          inqueue->status = STATUS_FILLING;
          //Then set the proper value for buflen
          if (debug)
            SerialUSB.println(tindex);

          buf_len = buf_len - tindex;
          tindex = buf_len + 1;

          if (debug)
            SerialUSB.println("Setting length");

          inqueue->len = 1;
          inqueue->packet[0] = apa_start;
        }
      }
      //check if we didn't find APA_START, then set the buffer length to 0.
      if (tindex != buf_len + 1) {
        buf_len = 0;
      }
    }
    //if the buffer is still larger than 0 then start reading in the data
    if (inqueue->status == STATUS_FILLING) {
      for (tindex = 0; tindex < buf_len; tindex++) {
        inqueue->packet[tindex + inqueue->len] = serialobj->read();
        if (inqueue->packet[tindex + inqueue->len] == apa_end) {
          if (debug)
            SerialUSB.println("Reached end of packet");
          inqueue->status = STATUS_FULL;
          inqueue->len = inqueue->len + tindex + 1;
          tindex = buf_len;
        }
      }
    }
    if (inqueue->status == STATUS_FILLING) {
      inqueue->len = inqueue->len + buf_len;
    }
    else if (debug && inqueue->status < STATUS_FILLING) {
      SerialUSB.println("Input Queue Ready");
      SerialUSB.print("\tLength: ");
      SerialUSB.println((int)inqueue->len);
      SerialUSB.print("\tContents: ");
      SerialUSB.write((char *)inqueue->packet, inqueue->len);
      SerialUSB.println("\n");
    }
  }
}

void checkOutputQueue(struct apa_port *outport, Stream *serialobj) {
  if (outport->packet_out->len > 0) {
    if (debug) {
      SerialUSB.print("Packet on Outqueue ");
      SerialUSB.print((char)outport->id);
      SerialUSB.print("\n");
      SerialUSB.print("\tLength: ");
      SerialUSB.println(outport->packet_out->len);
      SerialUSB.print("\tPayload:\n\n ");
      SerialUSB.write((char *)outport->packet_out->packet, outport->packet_out->len);
      SerialUSB.println("\n");
    }
    serialobj->write((char *)outport->packet_out->packet, outport->packet_out->len);
    outport->packet_out->len = 0;
  }
}


void checkOutputQueue(struct apa_port *outport, Stream *serialobj, Adafruit_ZeroDMA *dma) {
  if (outport->packet_out->status == STATUS_FULL) {
    dma->setup_transfer_descriptor(outport->packet_out->packet,
                                   (void *)(&SERCOM0->USART.DATA.reg),
                                   outport->packet_out->len,
                                   DMA_BEAT_SIZE_BYTE,
                                   true,
                                   false);
    dma->start_transfer_job();
  }
}

void local_process_packet(struct apa_port *port) {
  switch (port->payload_out[0]) {
    case 'p':
      ping_receive_time = micros();
      rainbowtime = rainbowtime + 1;
      ledChip.rainbow(rainbowtime);
      pinging = true;
      //SerialUSB.print("Ping: ");
      //SerialUSB.println(ping_receive_time - ping_send_time);

      //SerialUSB.println("us");
      avg_ping = avg_ping + 0.001 * (ping_receive_time - ping_send_time);
      ping_count++;
      ping_send_time = micros();

      port->path_out[0] = '^';
      port->path_out_length = 1;
      port->payload_out[0] = 'p';
      port->payload_out_length = 1;
#ifdef TESTPAYLOADLENGTH
      for (int i = 0; i < TESTPAYLOADLENGTH; i++)
        port->payload_out[i + 1] = (char)(65 + (i % 26));
      port->payload_out_length = TESTPAYLOADLENGTH + 1;
#endif
      break;
    case 's':
      ping_send_time = micros();
      pinging = true;
      apa_p_0->path_out[0] = '^';
      apa_p_0->path_out_length = 1;
      apa_p_0->payload_out[0] = 'p';
      apa_p_0->payload_out_length = 1;
#ifdef TESTPAYLOADLENGTH
      for (int i = 0; i < TESTPAYLOADLENGTH; i++)
        apa_p_0->payload_out[i + 1] = (unsigned char)(65 + (i % 26));
      apa_p_0->payload_out_length = TESTPAYLOADLENGTH + 1;
#endif
      port->path_out[0] = '^';
      port->path_out_length = 1;
      memcpy(port->payload_out, "Starting Ping Process", 21);
      port->payload_out_length = 21;
      break;
    default:
      SerialUSB.println("Packet Dropped");
      ping_send_time = micros();
      apa_p_0->path_out[0] = '^';
      apa_p_0->path_out_length = 1;
      apa_p_0->payload_out[0] = 'p';
      apa_p_0->payload_out_length = 1;

      apa_p_0->packet_in->len = 0;
      apa_p_0->packet_in->status = STATUS_EMPTY;
      break;
  }
}







