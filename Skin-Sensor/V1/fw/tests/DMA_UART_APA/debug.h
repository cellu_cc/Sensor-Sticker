void printPortStatus(struct apa_port *port){
  SerialUSB.print("Port ");
  SerialUSB.print((char)port->id);
  SerialUSB.println(":");
  
  SerialUSB.print("\tPacket In (");
  SerialUSB.print((int)port->packet_in->len);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->packet_in->packet,port->packet_in->len);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPath In (");
  SerialUSB.print((int)port->path_in_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->path_in,port->path_in_length);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPayload In (");
  SerialUSB.print((int)port->payload_in_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->payload_in, port->payload_in_length);
  SerialUSB.print("\n");

  SerialUSB.print("\tPacket Out (");
  SerialUSB.print((int)port->packet_out->len);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->packet_out->packet,port->packet_out->len);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPath Out (");
  SerialUSB.print((int)port->path_out_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->path_out,port->path_out_length);
  SerialUSB.print("\n");
  
  SerialUSB.print("\tPayload Out (");
  SerialUSB.print((int)port->payload_out_length);
  SerialUSB.print("): ");
  SerialUSB.write((char *)port->payload_out,port->payload_out_length);
  SerialUSB.print("\n");
  SerialUSB.print("\n");
}
