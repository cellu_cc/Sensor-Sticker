//#define TESTPAYLOADLENGTH (64)
#define APA_BAUD 1500000

typedef enum{
  NONE = 0,
  DEBUG,
  LOOP_DEBUG,
}verbose_mode;


verbose_mode verbosity = DEBUG;

//Input queue structs
struct apa_queue *apa_iq_0, *apa_iq_1, *apa_iq_2, *apa_iq_s; 
//Output queue structs
struct apa_queue *apa_oq_0, *apa_oq_1, *apa_oq_2, *apa_oq_s;

//APA port structs
struct apa_port *apa_p_0, *apa_p_1, *apa_p_2, *apa_p_s; 


void dma_callback_p0(struct dma_resource* const resource){
    apa_p_0->packet_out->status = STATUS_EMPTY;
    apa_p_0->packet_out->len = 0;
}

void dma_callback_p1(struct dma_resource* const resource){
    apa_p_1->packet_out->status = STATUS_EMPTY;
    apa_p_1->packet_out->len = 0;
}

void dma_callback_p2(struct dma_resource* const resource){
    apa_p_2->packet_out->status = STATUS_EMPTY;
    apa_p_2->packet_out->len = 0;
}

void initialize_network(){
  apa_iq_0 = apa_init_queue();
  apa_iq_1 = apa_init_queue();
  apa_iq_2 = apa_init_queue();
  apa_iq_s = apa_init_queue();

  apa_oq_0 = apa_init_queue();
  apa_oq_1 = apa_init_queue();
  apa_oq_2 = apa_init_queue();
  apa_oq_s = apa_init_queue();

  //Check if memory was successfully allocated
  if(verbosity <= DEBUG){
    if(apa_iq_0 == NULL || apa_iq_1 == NULL || apa_iq_2 == NULL || apa_iq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for input queues");
    if(apa_oq_0 == NULL || apa_oq_1 == NULL || apa_oq_2 == NULL || apa_oq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for output queues");
  }
  
  if (verbosity <= DEBUG)
    SerialUSB.println("Initializing APA Structs");
  //Initialize the ports with their IDs (using ASCII for now)
  apa_p_0 = apa_initialize('0');
  apa_p_1 = apa_initialize('1');
  apa_p_2 = apa_initialize('2');
  apa_p_s = apa_initialize('3');
  
  if (verbosity <= DEBUG)
    SerialUSB.println("Initializing APA Port Serials");
  APA0.begin(APA_BAUD, apa_p_0);
  APA1.begin(APA_BAUD, apa_p_1);
  APA2.begin(APA_BAUD, apa_p_2);

  if (verbosity <= DEBUG)
    SerialUSB.println("Registering Callbacks");

  APA0.registerCallback((dma_callback_t)dma_callback_p0);
  APA1.registerCallback((dma_callback_t)dma_callback_p1);
  APA2.registerCallback((dma_callback_t)dma_callback_p2);

  if (verbosity <= DEBUG)
    SerialUSB.println("Configuring Local Network");
  //configure local network (links between port objects)
  apa_p_0->next_port = apa_p_1;
  apa_p_1->next_port = apa_p_2;
  apa_p_2->next_port = apa_p_s;  
  apa_p_s->next_port = apa_p_0;  


  if (verbosity <= DEBUG)
    SerialUSB.println("Setting queue pointers");
  // pointing port queue I/O pointers to the queue objects      
  apa_p_0->packet_in = apa_iq_0;
  apa_p_1->packet_in = apa_iq_1;
  apa_p_2->packet_in = apa_iq_2;
  apa_p_s->packet_in = apa_iq_s;

  apa_p_0->packet_out = apa_oq_0;
  apa_p_1->packet_out = apa_oq_1;
  apa_p_2->packet_out = apa_oq_2;
  apa_p_s->packet_out = apa_oq_s;
}



