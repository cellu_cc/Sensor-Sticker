#include <apa.h>
#include <apa_phy.h>

#include "debug.h"
#include "network.h"
#include "peripherals.h"

uint32_t curtime;
uint32_t ping_time, ping_send_time, ping_receive_time;
uint32_t ping_interval = 1000;

uint32_t pval1, pval2;
uint32_t rtccount;
long avg_ping;
long ping_count;



uint8_t rainbowcount;

unsigned long rainbow_time, pressure_time, imu_time, rtc_time;
unsigned long beforetime, aftertime;
#define rainbow_interval 100
#define imu_interval 100
#define pressure_interval 100
#define rtc_interval 100

boolean pinging = false;

void setup() {
  //Starting USB Serial
  if (verbosity <= DEBUG)
    SerialUSB.begin(230400);

  peripheral_startup();
  //Setting up the APA network
  initialize_network();

  if (verbosity <= DEBUG)
    SerialUSB.println("Setting function pointers");
  //point to the local process function
  apa_p_0->process_packet = &local_process_packet;
  apa_p_1->process_packet = &local_process_packet;
  apa_p_2->process_packet = &local_process_packet;
  apa_p_s->process_packet = &local_process_packet;

  rainbow_time = 0;
  pressure_time = 0;
  imu_time = 0;

}

void loop() {
  curtime = millis();
  network_loop();

  if(curtime-rtc_time > rtc_interval){
    rtc_time = curtime;
    beforetime = micros();
    rtccount = rtc.getCount();
    aftertime = micros();
    SerialUSB.print("RTC Count get time: ");
    SerialUSB.println(aftertime-beforetime);
    beforetime = micros();
    aftertime = micros();
    aftertime = micros();
    SerialUSB.print("Micros() time: ");
    SerialUSB.println(aftertime-beforetime);
  }

  if(curtime - rainbow_time > rainbow_interval){
    rainbow_time = curtime;
    rainbowcount++;
    beforetime = micros();
    ledChip.rainbow(rainbowcount);
    aftertime = micros();
    SerialUSB.print("LED Chip Update Time: ");
    SerialUSB.println(aftertime-beforetime);
  }
  if(curtime - pressure_time > pressure_interval){
    pressure_time = curtime;
    beforetime = micros();
    pval1 = psense1.getCalibratedPressure();
    pval2 = psense2.getCalibratedPressure();
    aftertime = micros();
    SerialUSB.print("Pressure Sensor Read Time: ");
    SerialUSB.println(aftertime-beforetime);
    SerialUSB.print("\tPressure Vals: ");
    SerialUSB.print(pval1);
    SerialUSB.print(" ");
    SerialUSB.println(pval2);
  }
  if(curtime - imu_time > imu_interval){
    imu_time = curtime; 
    beforetime = micros();
    imu.readMag();
    imu.readGyro();
    imu.readAccel();
    aftertime = micros();
    SerialUSB.print("IMU Read Time: ");
    SerialUSB.println(aftertime-beforetime);
    SerialUSB.print("\tGyro Vals: ");
    SerialUSB.print(imu.calcGyro(imu.gx), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcGyro(imu.gy), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcGyro(imu.gz), 2);
    SerialUSB.println(" deg/s");
    SerialUSB.print("\tAccel Vals: ");
    SerialUSB.print(imu.calcAccel(imu.ax), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcAccel(imu.ay), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcAccel(imu.az), 2);
    SerialUSB.println(" g");  
    SerialUSB.print("\tMag Vals: ");
    SerialUSB.print(imu.calcMag(imu.mx), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcMag(imu.my), 2);
    SerialUSB.print(", ");
    SerialUSB.print(imu.calcMag(imu.mz), 2);
    SerialUSB.println(" gauss");
  }
}





void local_process_packet(struct apa_port *port) {
  switch (port->payload_out[0]) {
    case 'p':
      ping_receive_time = micros();
      rainbowcount = rainbowcount + 1;
      ledChip.rainbow(rainbowcount);
      pinging = true;
      //SerialUSB.print("Ping: ");
      //SerialUSB.println(ping_receive_time - ping_send_time);

      //SerialUSB.println("us");
      avg_ping = avg_ping + 0.001 * (ping_receive_time - ping_send_time);
      ping_count++;
      ping_send_time = micros();

      port->path_out[0] = '^';
      port->path_out_length = 1;
      port->payload_out[0] = 'p';
      port->payload_out_length = 1;
#ifdef TESTPAYLOADLENGTH
      for (int i = 0; i < TESTPAYLOADLENGTH; i++)
        port->payload_out[i + 1] = (char)(65 + (i % 26));
      port->payload_out_length = TESTPAYLOADLENGTH + 1;
#endif
      break;
    case 's':
      ping_send_time = micros();
      pinging = true;
      apa_p_0->path_out[0] = '^';
      apa_p_0->path_out_length = 1;
      apa_p_0->payload_out[0] = 'p';
      apa_p_0->payload_out_length = 1;
#ifdef TESTPAYLOADLENGTH
      for (int i = 0; i < TESTPAYLOADLENGTH; i++)
        apa_p_0->payload_out[i + 1] = (unsigned char)(65 + (i % 26));
      apa_p_0->payload_out_length = TESTPAYLOADLENGTH + 1;
#endif
      port->path_out[0] = '^';
      port->path_out_length = 1;
      memcpy(port->payload_out, "Starting Ping Process", 21);
      port->payload_out_length = 21;
      break;
    default:
      SerialUSB.println("Packet Dropped");
      ping_send_time = micros();
      apa_p_0->path_out[0] = '^';
      apa_p_0->path_out_length = 1;
      apa_p_0->payload_out[0] = 'p';
      apa_p_0->payload_out_length = 1;

      apa_p_0->packet_in->len = 0;
      apa_p_0->packet_in->status = STATUS_EMPTY;
      break;
  }
}







