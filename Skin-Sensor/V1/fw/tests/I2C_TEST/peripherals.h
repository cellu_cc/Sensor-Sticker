//Real time counter library
#include <RTC_CSL.h>

//LED controller library
#include <lp5521.h>
#define LP5521_ADD 0x32

//DPS310 Library
#include <dps310.h>
#define PSENSE1_ADD 0x76
#define PSENSE2_ADD 0x77

//LSM9DS1 (IMU) library
#include <SparkFunLSM9DS1.h>

// SDO_XM and SDO_G are both pulled high, so our addresses are:
#define LSM9DS1_M  0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW

//LED Chip object (for controlling RGB LED);
Lp5521 ledChip(LP5521_ADD);

//Initialize RTC module
RTC_CSL rtc;

//initialize DPS310 Sensors
DPS310 psense1(PSENSE1_ADD);
DPS310 psense2(PSENSE2_ADD);

//Initialize LSM9DS1
LSM9DS1 imu;

// LED Timing variables (used for indicator LED functions)
int startuptime;

void peripheral_startup(){
  if (verbosity <= DEBUG)
    SerialUSB.println("Starting RTC");
  rtc.begin();

  if (verbosity <= DEBUG)
    SerialUSB.println("Starting LED Chip");
  ledChip.Begin();

  if (verbosity <= DEBUG)
    Serial.println("Enabling LED");
  ledChip.Enable();

  //Configuring LED chip to have a lower drive current (less power/bright)
  ledChip.SetDriveCurrent(0, 10);
  ledChip.SetDriveCurrent(1, 10);
  ledChip.SetDriveCurrent(2, 10);

  //A pulsing indicator to show startup
  if (verbosity <= DEBUG) {
    while (startuptime < 500) {
      startuptime = startuptime + 1;
      ledChip.hum(startuptime % 255);
      delay(5);
    }
  }
  //Turn off the LED
  ledChip.hum(0);

  psense1.init();
  delay(100);
  psense1.enableDefaultSettings();

  psense2.init();
  delay(100);
  psense2.enableDefaultSettings();

  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;

  if (!imu.begin())
  {
    Serial.println("Failed to communicate with LSM9DS1.");
    Serial.println("Double-check wiring.");
    Serial.println("Default settings in this sketch will " \
                  "work for an out of the box LSM9DS1 " \
                  "Breakout, but may need to be modified " \
                  "if the board jumpers are.");
    while (1)
      ;
  }
}

