//#define TESTPAYLOADLENGTH (64)
#define APA_BAUD 1500000

typedef enum{
  NONE = 0,
  DEBUG,
  LOOP_DEBUG,
}verbose_mode;


verbose_mode verbosity = DEBUG;

unsigned char temp_input, buf_len, tindex;

boolean data_avail = false;

//Input queue structs
struct apa_queue *apa_iq_0, *apa_iq_1, *apa_iq_2, *apa_iq_s; 
//Output queue structs
struct apa_queue *apa_oq_0, *apa_oq_1, *apa_oq_2, *apa_oq_s;
//APA port structs
struct apa_port *apa_p_0, *apa_p_1, *apa_p_2, *apa_p_s; 


void dma_callback_p0(struct dma_resource* const resource){
    apa_p_0->packet_out->status = STATUS_EMPTY;
    apa_p_0->packet_out->len = 0;
}

void dma_callback_p1(struct dma_resource* const resource){
    apa_p_1->packet_out->status = STATUS_EMPTY;
    apa_p_1->packet_out->len = 0;
}

void dma_callback_p2(struct dma_resource* const resource){
    apa_p_2->packet_out->status = STATUS_EMPTY;
    apa_p_2->packet_out->len = 0;
}

void initialize_network(){
  apa_iq_0 = apa_init_queue();
  apa_iq_1 = apa_init_queue();
  apa_iq_2 = apa_init_queue();
  apa_iq_s = apa_init_queue();

  apa_oq_0 = apa_init_queue();
  apa_oq_1 = apa_init_queue();
  apa_oq_2 = apa_init_queue();
  apa_oq_s = apa_init_queue();

  //Check if memory was successfully allocated
  if(verbosity <= DEBUG){
    if(apa_iq_0 == NULL || apa_iq_1 == NULL || apa_iq_2 == NULL || apa_iq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for input queues");
    if(apa_oq_0 == NULL || apa_oq_1 == NULL || apa_oq_2 == NULL || apa_oq_s == NULL)
      SerialUSB.println("Memory not successfully allocated for output queues");
  }
  
  if (verbosity <= DEBUG)
    SerialUSB.println("Initializing APA Structs");
  //Initialize the ports with their IDs (using ASCII for now)
  apa_p_0 = apa_initialize('0');
  apa_p_1 = apa_initialize('1');
  apa_p_2 = apa_initialize('2');
  apa_p_s = apa_initialize('3');
  
  if (verbosity <= DEBUG)
    SerialUSB.println("Initializing APA Port Serials");
  APA0.begin(APA_BAUD, apa_p_0);
  APA1.begin(APA_BAUD, apa_p_1);
  APA2.begin(APA_BAUD, apa_p_2);

  if (verbosity <= DEBUG)
    SerialUSB.println("Registering Callbacks");

  APA0.registerCallback((dma_callback_t)dma_callback_p0);
  APA1.registerCallback((dma_callback_t)dma_callback_p1);
  APA2.registerCallback((dma_callback_t)dma_callback_p2);

  if (verbosity <= DEBUG)
    SerialUSB.println("Configuring Local Network");
  //configure local network (links between port objects)
  apa_p_0->next_port = apa_p_1;
  apa_p_1->next_port = apa_p_2;
  apa_p_2->next_port = apa_p_s;  
  apa_p_s->next_port = apa_p_0;  


  if (verbosity <= DEBUG)
    SerialUSB.println("Setting queue pointers");
  // pointing port queue I/O pointers to the queue objects      
  apa_p_0->packet_in = apa_iq_0;
  apa_p_1->packet_in = apa_iq_1;
  apa_p_2->packet_in = apa_iq_2;
  apa_p_s->packet_in = apa_iq_s;

  apa_p_0->packet_out = apa_oq_0;
  apa_p_1->packet_out = apa_oq_1;
  apa_p_2->packet_out = apa_oq_2;
  apa_p_s->packet_out = apa_oq_s;
}

void checkInputQueue(struct apa_queue *inqueue, Stream *serialobj) {
  //Check if packet is ready (i.e. have we reached APA_END char yet?)
  //Designed to allow packets > 64 bytes (i.e. larger than serial buffer)
  if (inqueue->status > STATUS_FULL) {
    //Get the number of available bytes
    buf_len = serialobj->available();

    if (buf_len > 0) {
      if (verbosity <= DEBUG) {
        SerialUSB.println("USB Data Available");
        SerialUSB.print("\tLength: ");
        SerialUSB.println((int)buf_len);
      }
    }

    //if the input queue is empty (nothing has been read in yet)...
    if (inqueue->status > STATUS_FILLING) {
      //then first search for APA_START
      tindex = 0;
      for (tindex = 0; tindex < buf_len; tindex++) {
        temp_input = serialobj->read();
        //if the next char in the serial buffer is APA_START...
        if (temp_input == apa_start) {
          if (verbosity <= DEBUG)
            SerialUSB.println("Found start of packet");
          inqueue->status = STATUS_FILLING;
          //Then set the proper value for buflen
          if (verbosity <= DEBUG)
            SerialUSB.println(tindex);

          buf_len = buf_len - tindex;
          tindex = buf_len + 1;

          if (verbosity <= DEBUG)
            SerialUSB.println("Setting length");

          inqueue->len = 1;
          inqueue->packet[0] = apa_start;
        }
      }
      //check if we didn't find APA_START, then set the buffer length to 0.
      if (tindex != buf_len + 1) {
        buf_len = 0;
      }
    }
    //if the buffer is still larger than 0 then start reading in the data
    if (inqueue->status == STATUS_FILLING) {
      for (tindex = 0; tindex < buf_len; tindex++) {
        inqueue->packet[tindex + inqueue->len] = serialobj->read();
        if (inqueue->packet[tindex + inqueue->len] == apa_end) {
          if (verbosity <= DEBUG)
            SerialUSB.println("Reached end of packet");
          inqueue->status = STATUS_FULL;
          inqueue->len = inqueue->len + tindex + 1;
          tindex = buf_len;
        }
      }
    }
    if (inqueue->status == STATUS_FILLING) {
      inqueue->len = inqueue->len + buf_len;
    }
    else if ((verbosity <= DEBUG) && inqueue->status < STATUS_FILLING) {
      SerialUSB.println("Input Queue Ready");
      SerialUSB.print("\tLength: ");
      SerialUSB.println((int)inqueue->len);
      SerialUSB.print("\tContents: ");
      SerialUSB.write((char *)inqueue->packet, inqueue->len);
      SerialUSB.println("\n");
    }
  }
}

void checkOutputQueue(struct apa_port *outport, Stream *serialobj) {
  if (outport->packet_out->len > 0) {
    if (verbosity <= DEBUG) {
      SerialUSB.print("Packet on Outqueue ");
      SerialUSB.print((char)outport->id);
      SerialUSB.print("\n");
      SerialUSB.print("\tLength: ");
      SerialUSB.println(outport->packet_out->len);
      SerialUSB.print("\tPayload:\n\n ");
      SerialUSB.write((char *)outport->packet_out->packet, outport->packet_out->len);
      SerialUSB.println("\n");
    }
    serialobj->write((char *)outport->packet_out->packet, outport->packet_out->len);
    outport->packet_out->len = 0;
  }
}

void network_loop(){
  if (apa_port_scan(apa_p_0) != SUCCESS && (verbosity <= DEBUG)) {
    SerialUSB.println("Failed Packet");
    printPortStatus(apa_p_0);
    apa_p_0->packet_in->len = 0;
    apa_p_0->packet_in->status = STATUS_EMPTY;
  }

  apa_port_scan(apa_p_1);
  apa_port_scan(apa_p_2);
  apa_port_scan(apa_p_s);

  //*/

  if (verbosity >= LOOP_DEBUG)
    SerialUSB.println("Checking Input Queues");


  checkInputQueue(apa_iq_s, &SerialUSB);


  if (verbosity >= LOOP_DEBUG)
    SerialUSB.println("Checking Output Queues");

  if (SerialUSB.available())
    checkOutputQueue(apa_p_s, &SerialUSB);

  APA0.checkOutputQueue();

  
  if (data_avail) {
    if (verbosity <= DEBUG)
      SerialUSB.println("Data Available.");
    APA0.IrqHandler();
  }
}



